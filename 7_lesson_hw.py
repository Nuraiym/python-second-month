import sqlite3

class Author:

    def __init__(self):
        self.connection = sqlite3.connect('db7.sqlite3')
        self.cursor = self.connection.cursor()

    def create_table(self):
        self.cursor.execute(
            "create table if not exists Author_7 (id integer Primary key, name text, surname text, magazine text)")
        self.connection.commit()

    def insert(self, id, name, surname, magazine):
        cursor = self.connection.cursor()
        cursor.execute("insert into Author_7 values (?, ?, ?, ?)", (id, name, surname, magazine))
        self.connection.commit()


author = Author()
author.create_table()
# author.insert(5, 'Nuraiym', 'S', 'Vogue')
# author.insert(2, 'Bekaiym', 'Sh', 'Forbes')
# author.insert(3, 'Aigerim', 'S', 'Vogue')


class Post():

    def __init__(self):
        self.connection = sqlite3.connect('db7.sqlite3')
        self.cursor = self.connection.cursor()

    def create_table(self):
        self.cursor.execute(
            '''create table if not exists Post_7(id integer Primary key, title text, date text, author_id integer
             references Author_7(id))''')
        self.connection.commit()
    #
    # def link(self):
    #     cursor = self.connection.cursor()
    #     cursor.execute("foreign key(Author_7_id) references author_7(id)")
    #     self.connection.commit()

    def insert(self, id, title, date, author_id):
        cursor = self.connection.cursor()
        cursor.execute("insert into Post_7 values (?, ?, ?, ?)", (id, title, date, author_id))
        self.connection.commit()


post = Post()
post.create_table()
post.insert(1, 'Audit', '20.20.20', 1)
post.insert(2, 'trendy_2020', '01.01.20', 1)
post.insert(3, 'Top 5 the richest people in the world', '01.05.20', 2)
post.insert(4, 'Top 5 the richest companies in the world', '01.06.20', 2)
post.insert(5, 'Trendy 2021', '01.01.2021', 3)
post.insert(6, 'Manadgment in companies', '01.06.20', 3)

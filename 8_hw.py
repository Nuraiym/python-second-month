import sqlite3


class Students:

    def __init__(self):
        self.connection = sqlite3.connect('db8.sqlite3')
        self.cursor = self.connection.cursor()

    def create_table(self):
        cursor = self.connection.cursor()
        cursor.execute("create table if not exists students(id integer Primary key, name text, surname text)")
        self.connection.commit()

    def insert(self, id, name, surname):
        cursor = self.connection.cursor()
        cursor.execute("insert into students values (?, ?, ?)", (id, name, surname))
        self.connection.commit()

    def delete(self, id):
        cursor = self.connection.cursor()
        cursor.execute(''' delete from students where id = ? ''', (id,))
        self.connection.commit()

    def m2m(self, second_table):
        cursor = self.connection.cursor()
        cursor.execute(f'''create table if not exists professor_students (professor_id integer  
        references {second_table} (id) on delete cascade, student_id integer references students(id) on delete cascade)
                       ''')

        self.connection.commit()

    def link(self, student_id, professor_id):
        cursor = self.connection.cursor()
        cursor.execute("insert into professor_students values (?, ?)", (student_id, professor_id))
        self.connection.commit()


students = Students()
students.create_table()
# students.insert(7, 'Daniiar', 'm')
# students.insert(8, 'Aidai', 'm')
# students.insert(3, 'Nuraiym', 'm')
# students.insert(4, 'Jalil', 'm')
# students.insert(5, 'Aisultan', 'm')

students.delete (7)
students.m2m("professor")
students.link(1, 1)
students.link(1, 2)
students.link(1, 3)


class Professor:

    def __init__(self):
        self.connection = sqlite3.connect('db8.sqlite3')
        self.cursor = self.connection.cursor()

    def create_table(self):
        cursor = self.connection.cursor()
        cursor.execute(
            "create table if not exists professor(id integer Primary key, name text, surname text, subject text)")
        self.connection.commit()

    def insert(self, id, name, surname, subject):
        cursor = self.connection.cursor()
        cursor.execute("insert into professor values (?, ?, ?, ?)", (id, name, surname, subject))
        self.connection.commit()


    def delete(self, id):
        cursor = self.connection.cursor()
        cursor.execute(''' delete from professor where id = ?''', (id,))
        self.connection.commit()


    def m2m(self, second_table):
        cursor = self.connection.cursor()
        cursor.execute(f'''create table if not exists professor_students 
        (student_id integer references {second_table} (id) on delete cascade,
         professor_id integer references professor (id) on delete cascade)
                               ''')
        self.connection.commit()

    def link(self, student_id, professor_id):
        cursor = self.connection.cursor()
        cursor.execute("insert into professor_students values (?, ?)", (student_id, professor_id))
        self.connection.commit()


professor = Professor()
professor.create_table()
# professor.insert(1, 'Nuraiym', 'S', 'finansy')
# professor.insert(2, 'Nursultan', 'S', 'zakony')

professor.delete(1)
professor.link(2, 2)
professor.link(2, 3)
professor.link(2, 4)
professor.link(2, 5)

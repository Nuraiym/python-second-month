class Factorial:

    def __init__(self, num):
        self.num = num

    def get_factorial(self, s=1):
        if self.num == 1:
            return s
        return Factorial.get_factorial(Factorial(self.num-1), self.num*s)

f = Factorial(7)


print(f.get_factorial())


# Сделать классами комплексные числа с магическими методами что бы они могли быть
# сложены +, -, *, / несколько раз (для это возращайте новый созданный объект
# класса Complex а не просто значение)

class Complex:
    def __init__(self, a, b):
        self.a = a
        self.b = b


    def __add__(self, other):
        first = (self.a + other.a)
        second = (self.b + other.b)
        return Complex (first, second)

    def __sub__ (self, other):
        first = (self.a - other.a)
        second = (self.b - other.b)
        return Complex (first, second)

    def __mul__(self, other):
        first = (self.a * other.a)
        second = (self.b * other.b)
        return Complex (first, second)

    def __truediv__(self, other):
        first = (self.a / other.a)
        second = (self.b / other.b)
        return Complex (first, second)




z1 = Complex(2, 3)
z2 = Complex(4, 5)
z3 = z1 + z2
z4 = z1 - z2
z5 = z1 * z2
z6 = z1 / z2
print(z3.a, z3.b)
print(z4.a, z4.b)
print(z5.a, z5.b)
print(z6.a, z6.b)

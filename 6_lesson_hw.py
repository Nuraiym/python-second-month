import sqlite3
class Products:

    def __init__(self):
        self.connection = sqlite3.connect('db6lessson.sqlite3')
        self.cursor = self.connection.cursor()


    def create_table(self):
        self.cursor.execute (""" 
        create table if not exists Products (id integer, name text, price integer)
        """)
        self.connection.commit()


    def insert(self, id, name, price):
        self.cursor.execute(""" 
        insert into Products values(?, ?, ?)
                """, (id, name, price))
        self.connection.commit()

    def update(self, id, name, price):
        self.cursor.execute(""" 
        update Products set name = ?, price = ?
        where id = ?
        """, (name, price, id))
        self.connection.commit()

    def delete(self, id):
        self.cursor.execute(""" 
        delete from Products where id = ? 
        """, (id,))
        self.connection.commit()


products = Products()
products.create_table()
products.insert(11, 'banan', 25)
products.insert(12, 'salmon', 45)
products.insert(13, 'lemon', 22)
products.insert(14, 'bread', 12)
products.insert(15, 'milk', 4)
products.insert(16, 'butter', 18)

products.update(15, name ='soy souce', price = 6)
products.delete(12)









